import java.util.Random;
public class Die{
	//Fields 
	private int faceValue;
	private Random giveValue;
	
	//Constructor
	public Die(){
		this.faceValue = 1;
		this.giveValue = new Random();
	}
	
	//Getter
	public int getFaceValue(){
		return this.faceValue;
	}
	
	//roll method 
	public void roll(){
		this.faceValue = this.giveValue.nextInt(6) + 1;
	}
	
	//toString
	public String toString(){
		return "Die value: " + this.faceValue;
	}
}