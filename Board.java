public class Board{
	//Fields 
	private Die dice1;
	private Die dice2;
	private boolean[] tiles;
	
	//Constructor
	public Board(){
		this.dice1 = new Die();
		this.dice2 = new Die();
		this.tiles = new boolean[12];
	}
	
	//playATurn
	public boolean playATurn(){
		this.dice1.roll();
		System.out.println(this.dice1);
		this.dice2.roll();
		System.out.println(this.dice2);     
		int sumOfDice = this.dice1.getFaceValue() + this.dice2.getFaceValue();
		if (this.tiles[sumOfDice-1] == false){
			this.tiles[sumOfDice-1] = true;
			System.out.println("Closing tile equal to sum: " + sumOfDice);
			return false;
		}
		else if(this.tiles[this.dice1.getFaceValue()-1] == false){
			this.tiles[this.dice1.getFaceValue()-1] = true;
			System.out.println("Closing tile with the same value as die 1: " + this.dice1.getFaceValue());
			return false;
		}
		else if(this.tiles[this.dice2.getFaceValue()-1] == false){
			this.tiles[this.dice2.getFaceValue()-1] = true;
			System.out.println("Closing tile with the same value as die 2: " + this.dice2.getFaceValue());
			return false;
		}
		else{
			System.out.println("All the tiles for these values are already shut");
			return true;
		}
		
	}
	
	//toString
	public String toString(){
		String[] printValue = new String[12];
		String value = "";
		for (int i = 0; i<this.tiles.length; i++){
		if (this.tiles[i] == true){
			printValue[i] = "X ";
		}
		else{
			int temp = i + 1;
			printValue[i] = "" + temp + " ";
		}
		value += printValue[i];
		}
		return value;

	}

}
