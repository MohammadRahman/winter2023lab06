import java.util.Scanner;
public class Jackpot{
	//Main method (Bonus question coded here)
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
	    int wins = playOneRound();
        System.out.println("You won " + wins + " game, would you like to play another Jackpot round? (yes or no)");	
		String exitJackpot = reader.next();
		boolean continueGame = true;
		while (continueGame != false){
		if (exitJackpot.equals("yes")){
			wins += playOneRound();
			System.out.println("You won " + wins + " games, would you like to play another Jackpot round? (yes or no)");	
		    exitJackpot = reader.next();
		}
		else if (exitJackpot.equals("no")){
			System.out.println("Thank you for playing!");
			continueGame = false;
		    }
			else {
				System.out.println("Invalid answer, leaving the game...");
				continueGame = false;
			}
		}
	}
	
	//playOneRound method
	public static int playOneRound(){
		System.out.println("WELCOME ABOARD! HERE ARE THE RESULTS OF YOUR MOST RECENT JACKPOT GAME:");
		Board board = new Board();
		boolean gameOver = false;
		int numOfTilesClosed = 0;
		int gamesWon = 0;
		while (gameOver != true){
			System.out.println(board);
			gameOver = board.playATurn();
			if (gameOver != true){
				numOfTilesClosed++;
			}
		}
		System.out.println("Number of tiles closed: " + numOfTilesClosed);
		if (numOfTilesClosed>=7){
			System.out.println("You reached the jackpot and won!");
			gamesWon++;
		}
		else {
			System.out.println("You lost! Good luck next time!");
		}
		return gamesWon;
			
	}
}